package com.example.demo4.controller;

import java.util.logging.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
class Demo4Controller{
	
	private static final Logger LOG = Logger.getLogger(Demo4Controller.class.getName());

	@GetMapping(value="/demo4")
	public String demoService1() {
		LOG.info("Inside demo service 4...");
		String response = "Visited demo service 4.\n";
		return response;
	}
}
